import logging
import time
from http import HTTPStatus
from typing import Callable

from aiohttp import web, web_exceptions
from aiohttp.web import Request, Response, middleware

from app import errors

ERROR = 'error'


@middleware
async def log_middleware(request: Request, handler: Callable) -> Response:  # noqa: WPS110
    """Логирование для каждого запроса."""
    start_time = time.time()

    response = await handler(request)

    logging.info({
        'endpoint': f'{request.method} {request.path}',
        'query': request.query_string,
        'body': await request.text(),
        'time': f'{(time.time() - start_time):.3f}',
        'status_code': response.status,
        'response': response.text,
    })
    return response


@middleware
async def error_middleware(request: Request, handler: Callable) -> Response:  # noqa: WPS110
    """Обработка ошибок для каждого запроса."""
    try:
        return await handler(request)
    except web_exceptions.HTTPException as http_err:
        logging.exception({ERROR: http_err.reason})
        return web.json_response(
            {ERROR: http_err.reason},
            status=http_err.status,
        )
    except errors.ServiceError as service_err:
        logging.exception({ERROR: str(service_err)})
        return web.json_response(
            {ERROR: str(service_err)},
            status=service_err.http_code,
        )
    except Exception as error:
        error_text = 'Serious unexpected error'
        logging.exception({ERROR: error})
        return web.json_response(
            {ERROR: error_text},
            status=HTTPStatus.INTERNAL_SERVER_ERROR.value,
        )
