import sys
from typing import List, NoReturn

from aiohttp import web
from aiohttp_apispec import setup_aiohttp_apispec

from app.api.routes import setup_routes
from app.setup_service.configurator import get_config
from app.setup_service.logs import init_logger
from app.setup_service.middlewares import error_middleware, log_middleware


def start(args: List[str]) -> NoReturn:
    """Настроить и запустить сервер."""
    app = web.Application(
        middlewares=[log_middleware, error_middleware],
    )
    app['config'] = get_config(args)
    setup_routes(app)
    init_logger(app['config'])

    service_config = app['config']['service']
    setup_aiohttp_apispec(
        app=app,
        title=f"{service_config['name']} service API",
        version=service_config['version'],
        url='/api/docs/api-docs',
    )
    web.run_app(
        app,
        port=service_config['port'],
        access_log=None,
    )


if __name__ == '__main__':
    start(sys.argv[1:])
