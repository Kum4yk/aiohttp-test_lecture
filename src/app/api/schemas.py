from marshmallow import Schema, fields


class NameReqSchema(Schema):
    """Схема запроса демо-хендлера."""

    name = fields.Str(required=True)


class VersionRespSchema(Schema):
    """Схема ответа версии."""

    version = fields.Str(required=True)
