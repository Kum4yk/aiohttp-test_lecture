import aiohttp_cors
from aiohttp import web

from app.api.handlers import get_version, handle_example


def setup_routes(app: web.Application) -> None:
    """Настроить мапинг путей и разрешить междоменное взаимодействие."""
    cors = aiohttp_cors.setup(app, defaults={
        '*': aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers='*',
            allow_headers='*',
        ),
    })

    app.router.add_route('GET', '/', handle_example)
    app.router.add_route('GET', '/{name}', handle_example)
    app.router.add_route('GET', '/api/version', get_version)

    for route in list(app.router.routes()):
        cors.add(route)
