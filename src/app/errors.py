from http import HTTPStatus


class ServiceError(Exception):
    """Базовый класс для ошибок приложения."""

    def __init__(
        self,
        message: str = 'Unexpected error',
        http_code: int = HTTPStatus.INTERNAL_SERVER_ERROR,
    ):
        """Создает экземпляр Service Error.

        :param http_code: http код ошибки
        :param message: сообщение ошибки
        """
        super().__init__(message)
        self.http_code = http_code
        self.message = message

    def as_json_obj(self):
        """Получить json представление объекта ошибки."""
        return {'http_status': self.http_code, 'message': self.message}


class InvalidRequestError(ServiceError):
    """Базовый класс для ошибок связанных с некорректными входными запросами."""

    def __init__(  # noqa: WPS612
        self,
        message: str = 'Invalid request',
        http_code: int = HTTPStatus.BAD_REQUEST,
    ):
        """Создает экземпляр Invalid Request Error с соответствующим сообщением по умолчанию."""
        super().__init__(message, http_code)
