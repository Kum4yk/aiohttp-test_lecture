# Шаблон проекта

### Оглавление
* [Менеджер пакетов poetry](#менеджер-пакетов-poetry)
* [Простой web-сервер aiohttp](#простой-web-сервер-aiohttp)
* [Cross-Origin Resource Sharing](#cross-origin-resource-sharing)
* [Конфигурационные файлы](#конфигурационные-файлы)
* [Логирование](#логирование)
* [Middleware](#middleware)
* [Валидация API](#валидация-api)
* [Swagger](#swagger)


## Менеджер пакетов poetry

В виртуальном окружении установим `pip install poetry` и инициализируем `poetry init`.  
Появится файл [pyproject.toml](file://pyproject.toml), в нём можно указывать нужные нам зависимости.  

Добавим aiohttp и pytest с помощью `poetry add ...` или отредактируем pyproject.toml и выполним `poetry install`. Появится файл [poetry.lock](file://poetry.lock).

Подробнее смотрите [python-poetry.org](https://python-poetry.org/).


## Простой web-сервер aiohttp

Возьмём за основу [Getting Started](https://docs.aiohttp.org/en/stable/index.html#getting-started)


## Cross-Origin Resource Sharing

Разрешим междоменные запросы для всех источников и типов с помощью [aiohttp_cors](https://github.com/aio-libs/aiohttp-cors)

## Конфигурационные файлы

Научимся читать конфиги с помощью [trafaret-config](https://github.com/tailhook/trafaret-config).  
И запишем его в экземпляр Application
[(aiohttp application’s config)](https://docs.aiohttp.org/en/stable/web_advanced.html#application-s-config).

## Логирование

Настроим формат [логирования](https://docs.python.org/3/library/logging.html). 
Отключим стандартное логирование в aiohttp, взамен в хендлерах будем логировать:
* метод запроса и путь;
* параметры запроса;
* тело запроса;
* время обработки запроса;
* http код ответа;
* тело ответа;

## Middleware

Добавим [middleware](https://docs.aiohttp.org/en/stable/web_advanced.html#middlewares) для логгирования и обработки ошибок.

Создадим базовый класс ошибок сервиса, добавим примеры его использования.

## Валидация API

ДЛя валидации запросов будем использовать [webargs](https://webargs.readthedocs.io/en/latest/framework_support.html#aiohttp) в связке с [marshmallow](https://webargs.readthedocs.io/en/latest/framework_support.html#aiohttp).

Валидация ответа с помощью [marshmallow](https://marshmallow.readthedocs.io/en/stable/quickstart.html#validation-without-deserialization).

## Swagger

Настроим [aiohttp-apispec](https://aiohttp-apispec.readthedocs.io/en/latest/usage.html#quickstart), который будет генерировать swagger, просматривать который можно на [editor.swagger.io](https://editor.swagger.io/)

## wemake-python-styleguide

Установим и запустим [линтер](https://wemake-python-stylegui.de/en/latest/index.html).
С некоторыми его настройками и правилами мы можем быть не согласны, настроим конфигурационный файл [setup.cfg](https://wemake-python-stylegui.de/en/latest/pages/usage/configuration.html).

Подробнее о проверках линтера: [usage/violations](https://wemake-python-stylegui.de/en/latest/pages/usage/violations/)

## Тесты

Будем тестировать aiohttp сервис с помощью [pytest-aiohttp](https://docs.aiohttp.org/en/stable/testing.html#pytest)